package com.itbulat.institutions.configurations;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;

import com.itbulat.institutions.filters.AuthFilter;



@EnableWebSecurity
public class WebSecurityConfig {
	
	Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);
	
	@Autowired
	JavaEndpointsConfig javaEndPointsConfig;
	
	@Autowired
	AuthFilter authFilter;
	
	
	@Bean
	public SecurityFilterChain getResourcesFilterChain(HttpSecurity http) throws Exception
	{
		http.requestMatchers((request) -> { request.antMatchers("**"); })
		.authorizeRequests().anyRequest().authenticated()
		.and()
		.csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.anonymous().disable()
		.addFilterBefore(authFilter, ExceptionTranslationFilter.class);
		return http.build();
	}
	
	
	
}