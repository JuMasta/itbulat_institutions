package com.itbulat.institutions.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
public class JavaEndpointsConfig {
	
	@Value("${auth.server.url}")
	private String authServerUrl;
	
	
	
	public String getAuthServerUrl()
	{
		return authServerUrl;
	}
}
