package com.itbulat.institutions.filters;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.HashSet;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.itbulat.institutions.repositories.UserRepository;
import com.itbulat.institutions.configurations.JavaEndpointsConfig;
import com.itbulat.institutions.entities.User;

@Component
public class AuthFilter extends OncePerRequestFilter {

	Logger logger = LoggerFactory.getLogger(AuthFilter.class);

	@Autowired
	UserRepository userRepository;

	@Autowired
	private JavaEndpointsConfig javaEndPointsConfig;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String authorizationHeader = request.getHeader("Authorization");
		String login = null;
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			String token = authorizationHeader.substring(7);
			try {
				login = AuthenticateRequest(token);
			} catch (IOException | InterruptedException | BadCredentialsException e) {

				logger.error(e.getMessage());
//				response.sendError(500);

			}
		}
		if (login != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			User user = userRepository.findByPhoneNumber(login);

			UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getPhoneNumber(),
					user.getPassword(), new HashSet<>());

			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, null, userDetails.getAuthorities());
			usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

		}
		filterChain.doFilter(request, response);

	}

	private String AuthenticateRequest(String token) throws IOException, InterruptedException, BadCredentialsException {

		HttpClient client = HttpClient.newHttpClient();
		HttpRequest authRequest = HttpRequest.newBuilder(URI.create(javaEndPointsConfig.getAuthServerUrl()))
				.header("Authorization", "Bearer " + token).POST(BodyPublishers.ofString("")).build();

		HttpResponse<String> authResponse = null;
		authResponse = client.send(authRequest, BodyHandlers.ofString());
		int responseStatusCode = authResponse.statusCode();

		if (responseStatusCode == 200) {
			HttpHeaders responseHeaders = authResponse.headers();
			String login = responseHeaders.allValues("login").get(0);

			return login;
		} else
			throw new BadCredentialsException("invalid token, status: " + responseStatusCode);

	}

}
