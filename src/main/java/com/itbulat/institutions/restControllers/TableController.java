package com.itbulat.institutions.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbulat.institutions.dto.TableDTO;
import com.itbulat.institutions.entities.Table;
import com.itbulat.institutions.exceptions.BadActionException;
import com.itbulat.institutions.exceptions.UserProhibitedActionException;
import com.itbulat.institutions.services.TableService;

@RestController
@RequestMapping("table")
public class TableController {

	@Autowired
	TableService tableService;

	@PostMapping
	public ResponseEntity<?> createProduct(@RequestBody TableDTO tableDTO) {

		Table table;
		try {
			table = tableService.create(tableDTO);
		} catch (UserProhibitedActionException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
		}

		return new ResponseEntity<Table>(table, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getProductById(@PathVariable long id) {
		return new ResponseEntity<Table>(tableService.getById(id), HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Table>> getProductById() {
		return new ResponseEntity<List<Table>>(tableService.getAll(), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable long id) {
		try {
			tableService.deleteById(id);
		} catch (UserProhibitedActionException | BadActionException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
