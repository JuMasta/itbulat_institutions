package com.itbulat.institutions.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbulat.institutions.dto.ProductDTO;
import com.itbulat.institutions.entities.Product;
import com.itbulat.institutions.exceptions.BadActionException;
import com.itbulat.institutions.exceptions.UserProhibitedActionException;
import com.itbulat.institutions.services.ProductService;

@RestController
@RequestMapping("product")
public class ProductController {

	@Autowired
	ProductService productService;

	@PostMapping
	public ResponseEntity<?> createProduct(@RequestBody ProductDTO productDTO) {

		Product product;
		try {
			product = productService.create(productDTO);
		} catch (UserProhibitedActionException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
		}

		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getProductById(@PathVariable long id) {
		return new ResponseEntity<Product>(productService.getById(id), HttpStatus.OK);
	}
	
	@GetMapping("/")
	public ResponseEntity<List<Product>> getProductById() {
		return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
	}
	

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable long id) {
		try {
			productService.deleteById(id);
		} catch (UserProhibitedActionException | BadActionException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
