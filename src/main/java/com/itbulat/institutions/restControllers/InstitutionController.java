package com.itbulat.institutions.restControllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itbulat.institutions.dto.InstitutionDTO;
import com.itbulat.institutions.entities.Institution;
import com.itbulat.institutions.services.InstitutionService;

@RestController
@RequestMapping("institution")
public class InstitutionController {

	@Autowired
	InstitutionService institutionService;

	@GetMapping("/{id}")
	public ResponseEntity<Institution> getInstitution(@PathVariable long id) {
		Institution institution = institutionService.getById(id);
		return new ResponseEntity<Institution>(institution, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Institution>> findAll(@RequestParam Map<String, String> allParams) {

		List<Institution> list = institutionService.getAll(allParams);
		return new ResponseEntity<List<Institution>>(list, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Institution> createInstitution(@RequestBody InstitutionDTO institutionDTO) {
		Institution institution = institutionService.create(institutionDTO);
		return new ResponseEntity<Institution>(institution, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteInstitution(@PathVariable long id) {

		institutionService.deleteById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
