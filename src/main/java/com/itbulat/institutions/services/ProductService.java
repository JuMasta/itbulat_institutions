package com.itbulat.institutions.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itbulat.institutions.dto.ProductDTO;
import com.itbulat.institutions.entities.Product;
import com.itbulat.institutions.entities.User;
import com.itbulat.institutions.entities.UserInstitution;
import com.itbulat.institutions.exceptions.BadActionException;
import com.itbulat.institutions.exceptions.UserProhibitedActionException;
import com.itbulat.institutions.repositories.ProductRepository;
import com.itbulat.institutions.repositories.UserInstitutionRepository;
import com.itbulat.institutions.repositories.UserRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	UserInstitutionRepository userInstitutionRepository;

	@Autowired
	UserRepository userRepository;

	public Product create(ProductDTO productDTO) throws UserProhibitedActionException {
		User user = getAuthenticatedUser();
		checkAccessForAction(user.getId(), productDTO.getInstituionId());
		Product product = new Product();
		BeanUtils.copyProperties(productDTO, product);
		return productRepository.save(product);

	}

	public Product getById(long productId) {
		return productRepository.findById(productId);
	}
	
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	public void deleteById(long productId) throws UserProhibitedActionException, BadActionException {

		User user = getAuthenticatedUser();
		Product product = productRepository.findById(productId);
		
		if(product == null)
			throw new BadActionException("Wrong action");
		checkAccessForAction(user.getId(), product.getInstituionId());
		productRepository.deleteById(productId);
	}

	private void checkAccessForAction(long userId, long institutionId) throws UserProhibitedActionException {
		UserInstitution userInstitution = new UserInstitution();
		userInstitution.setInstitutionId(userId);
		userInstitution.setUserId(institutionId);

		List<UserInstitution> userInstitutionsList = userInstitutionRepository.findByUserId(userId);
		if (!userInstitutionsList.contains(userInstitution)) {
			throw new UserProhibitedActionException("Prohibited action");
		}
	}

	private User getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		User authenticatedUser = userRepository.findByPhoneNumber(login);

		return authenticatedUser;
	}



}
