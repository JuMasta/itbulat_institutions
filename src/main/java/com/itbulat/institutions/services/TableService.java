package com.itbulat.institutions.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itbulat.institutions.dto.TableDTO;
import com.itbulat.institutions.entities.Table;
import com.itbulat.institutions.entities.User;
import com.itbulat.institutions.entities.UserInstitution;
import com.itbulat.institutions.exceptions.BadActionException;
import com.itbulat.institutions.exceptions.UserProhibitedActionException;
import com.itbulat.institutions.repositories.TableRepository;
import com.itbulat.institutions.repositories.UserInstitutionRepository;
import com.itbulat.institutions.repositories.UserRepository;

@Service
public class TableService {

	@Autowired
	TableRepository tableRepository;
	
	@Autowired
	UserInstitutionRepository userInstitutionRepository;

	@Autowired
	UserRepository userRepository;
	
	
	public Table create(TableDTO tableDTO) throws UserProhibitedActionException
	{
		User user = getAuthenticatedUser();
		checkAccessForAction(user.getId(), tableDTO.getInstitutionId());
		Table table = new Table();
		BeanUtils.copyProperties(tableDTO, table);
		return tableRepository.save(table);
	}
	
	public Table getById(long tableId)
	{
		return tableRepository.findById(tableId);
	}
	
	public List<Table> getAll()
	{
		return tableRepository.findAll();
	}
	
	public void deleteById(long tableId) throws BadActionException, UserProhibitedActionException
	{
		User user = getAuthenticatedUser();
		Table table = tableRepository.findById(tableId);
		
		if (table == null)
			throw new BadActionException("Wrong action");
		checkAccessForAction(user.getId(), table.getInstitutionId());
		tableRepository.deleteById(tableId);
		
	}
	
	private void checkAccessForAction(long userId, long institutionId) throws UserProhibitedActionException {
		UserInstitution userInstitution = new UserInstitution();
		userInstitution.setInstitutionId(userId);
		userInstitution.setUserId(institutionId);

		List<UserInstitution> userInstitutionsList = userInstitutionRepository.findByUserId(userId);
		if (!userInstitutionsList.contains(userInstitution)) {
			throw new UserProhibitedActionException("Prohibited action");
		}
	}
	
	private User getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		User authenticatedUser = userRepository.findByPhoneNumber(login);

		return authenticatedUser;
	}

}
