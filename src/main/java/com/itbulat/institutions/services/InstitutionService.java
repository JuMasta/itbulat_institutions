package com.itbulat.institutions.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.itbulat.institutions.dto.InstitutionDTO;
import com.itbulat.institutions.entities.Institution;
import com.itbulat.institutions.entities.User;
import com.itbulat.institutions.entities.UserInstitution;
import com.itbulat.institutions.repositories.InstitutionRepository;
import com.itbulat.institutions.repositories.InstitutionRepositoryImpl;
import com.itbulat.institutions.repositories.UserInstitutionRepository;
import com.itbulat.institutions.repositories.UserRepository;
import com.itbulat.institutions.searchСriteria.SearchCriteria;

@Service
public class InstitutionService {

	Logger logger = LoggerFactory.getLogger(InstitutionService.class);

	@Autowired
	InstitutionRepository institutionRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserInstitutionRepository userInstitutionRepository;

	@Autowired
	InstitutionRepositoryImpl institutionRepositoryCustomImpl;

	Pattern pattern = Pattern.compile("(eq|gt|lt)\\s*'(.*)'");

	public Institution create(InstitutionDTO institutionDTO) {

		Institution institution = new Institution();
		BeanUtils.copyProperties(institutionDTO, institution);
		institution = institutionRepository.save(institution);

		User authenticatedUser = getAuthenticatedUser();
		createUserInstitutionRecord(authenticatedUser, institution);

		return institution;
	}

	public Institution getById(long institutionId) {
		return institutionRepository.findById(institutionId);
	}

	public void deleteById(long institutionId) {
		Institution institution = institutionRepository.findById(institutionId);
		if (institution != null)
			institutionRepository.deleteById(institution.getId());
	}

	public List<Institution> getAll(Map<String, String> allParams) {

		return institutionRepositoryCustomImpl.findInstitutionsByCriterias(getCriteriasFromParams(allParams));
	}

	private List<SearchCriteria> getCriteriasFromParams(Map<String, String> allParams) {
		Stream<Map.Entry<String, String>> entriesStream = allParams.entrySet().stream();
		List<SearchCriteria> criteriasList = new ArrayList<>();
		entriesStream.forEach((item) -> {
			Matcher matcher = pattern.matcher(item.getValue());
			if (matcher.find())
				criteriasList.add(new SearchCriteria(item.getKey(), matcher.group(1), matcher.group(2)));
		});

		return criteriasList;
	}

	private User getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		User authenticatedUser = userRepository.findByPhoneNumber(login);

		return authenticatedUser;
	}

	private void createUserInstitutionRecord(User authenticatedUser, Institution institution) {
		UserInstitution userInstitution = new UserInstitution();
		userInstitution.setInstitutionId(institution.getId());
		userInstitution.setUserId(authenticatedUser.getId());
		userInstitutionRepository.save(userInstitution);
	}

}
