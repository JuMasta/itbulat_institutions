package com.itbulat.institutions.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itbulat.institutions.entities.Category;
import com.itbulat.institutions.repositories.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	
	public Category getById(long categoryId)
	{
		return categoryRepository.findById(categoryId);
	}
	
}
