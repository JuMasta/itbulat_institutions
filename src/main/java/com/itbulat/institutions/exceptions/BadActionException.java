package com.itbulat.institutions.exceptions;

public class BadActionException extends Exception {

	public BadActionException(String message) {
		super(message);
	}

}
