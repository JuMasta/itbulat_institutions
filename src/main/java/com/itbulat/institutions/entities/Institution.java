package com.itbulat.institutions.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.itbulat.institutions.annotations.Filter;

import lombok.Data;
import lombok.ToString;


@Entity
@Table(name="institutions")
@ToString
@Data
public class Institution {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Filter
	String name;
	
	@Filter
	String address;
	
	
}
