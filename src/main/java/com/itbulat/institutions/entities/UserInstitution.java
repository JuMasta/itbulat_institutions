package com.itbulat.institutions.entities;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "user_institution")
@ToString
@Data
public class UserInstitution {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "instituion_id")
	private long institutionId;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInstitution other = (UserInstitution) obj;
		return institutionId == other.institutionId && userId == other.userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(institutionId, userId);
	}

}
