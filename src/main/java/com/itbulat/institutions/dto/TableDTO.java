package com.itbulat.institutions.dto;


import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class TableDTO {

	@NotEmpty
	private int number;

	@NotEmpty
	private int size;

	@NotEmpty
	private int possition;

	@NotEmpty
	private long institutionId;
	
}
