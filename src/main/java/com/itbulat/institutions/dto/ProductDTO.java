package com.itbulat.institutions.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ProductDTO {
	
	@NotEmpty
	private String name;
	
	@NotEmpty
	private BigDecimal price;
	
	@NotEmpty
	private long instituionId;

}
