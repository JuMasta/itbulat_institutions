package com.itbulat.institutions.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class InstitutionDTO {
	
	@NotEmpty
	String name;
	
	@NotEmpty
	String address;
	
}
