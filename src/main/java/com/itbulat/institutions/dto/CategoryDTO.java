package com.itbulat.institutions.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class CategoryDTO {

	@NotEmpty
	private String name;

	@NotEmpty
	private long instituionId;

}
