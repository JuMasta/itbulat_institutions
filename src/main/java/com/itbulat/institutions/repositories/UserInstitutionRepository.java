package com.itbulat.institutions.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.institutions.entities.UserInstitution;

public interface UserInstitutionRepository extends CrudRepository<UserInstitution, Long> {
	
	
	UserInstitution save(UserInstitution userInstitution);
	
	List<UserInstitution> findByUserId(long userID);

}
