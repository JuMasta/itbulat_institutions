package com.itbulat.institutions.repositories;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.itbulat.institutions.annotations.Filter;
import com.itbulat.institutions.entities.Institution;
import com.itbulat.institutions.searchСriteria.SearchCriteria;

@Component
public class InstitutionRepositoryImpl implements InstitutionRepositoryCustom {

	Logger logger = LoggerFactory.getLogger(InstitutionRepositoryImpl.class);
	
	List<String> filteredFelds;

	@PersistenceContext
	private EntityManager entityManager;

	public InstitutionRepositoryImpl() {
		Class<Institution> institutionClass = Institution.class;

		Stream<Field> fieldsStream = Arrays.stream(institutionClass.getDeclaredFields());

		this.filteredFelds = fieldsStream.filter((field) -> field.getAnnotation(Filter.class) != null)
				.map((field) -> field.getName()).collect(Collectors.toList());
	}
	
	@Override
	public List<Institution> findInstitutionsByCriterias(List<SearchCriteria> list) {

		Stream<SearchCriteria> criteriasStream = list.stream();
		List<SearchCriteria> criteriasList = criteriasStream
				.filter((item) -> this.filteredFelds.contains(item.getKey())).collect(Collectors.toList());

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Institution> query = cb.createQuery(Institution.class);

		Root<Institution> institution = query.from(Institution.class);

		List<Predicate> predicates = new ArrayList<>();

		for (SearchCriteria criteria : criteriasList) {
			Predicate predicate = getPredicateByCriteria(criteria, cb, institution);
			if (predicate != null)
				predicates.add(predicate);
		}

		query.select(institution).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		return entityManager.createQuery(query).getResultList();

	}

	private Predicate getPredicateByCriteria(SearchCriteria criteria, CriteriaBuilder cb,
			Root<Institution> institution) {
		if (criteria.getOperation().equalsIgnoreCase("gt")) {
			return cb.greaterThanOrEqualTo(institution.<String>get(criteria.getKey()), criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase("lt")) {
			return cb.lessThanOrEqualTo(institution.<String>get(criteria.getKey()), criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase("eq")) {
			if (institution.get(criteria.getKey()).getJavaType() == String.class) {
				return cb.like(institution.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
			} else {
				return cb.equal(institution.get(criteria.getKey()), criteria.getValue());
			}
		}
		return null;
	}

}
