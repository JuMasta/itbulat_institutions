package com.itbulat.institutions.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.institutions.entities.Table;

public interface TableRepository extends CrudRepository<Table, Long> {

	Table findById(long tableId);
	
	List<Table> findAll();
	
}
