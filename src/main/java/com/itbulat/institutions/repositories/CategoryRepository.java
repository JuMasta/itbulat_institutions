package com.itbulat.institutions.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.institutions.entities.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

	Category findById(long categoryId);
}
