package com.itbulat.institutions.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.institutions.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User save(User user);
	
	User findByid(Long id);
	
	User findByPhoneNumber(String phoneNumber);
	
	List<User> findAll();
	
	void deleteById(long id);
	
}
