package com.itbulat.institutions.repositories;

import java.util.List;

import com.itbulat.institutions.entities.Institution;
import com.itbulat.institutions.searchСriteria.SearchCriteria;

public interface InstitutionRepositoryCustom {

	List<Institution> findInstitutionsByCriterias(List<SearchCriteria> searchCriterias);

}
