package com.itbulat.institutions.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.institutions.entities.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	Product findById(long id);
	
	List<Product> findAll();
	
}
