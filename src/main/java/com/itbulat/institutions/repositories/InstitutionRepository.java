package com.itbulat.institutions.repositories;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itbulat.institutions.entities.Institution;


public interface InstitutionRepository extends JpaRepository<Institution, Long> {

	Institution findById(long id);
	
	List<Institution> findAll(Specification<Institution> specification);
	
	void deleteById(long id);
}
