FROM gradle:7.3.3-jdk17-alpine AS build

COPY ./ ./

RUN gradle build

FROM openjdk:17

WORKDIR app

COPY --from=build /home/gradle/build/libs/ .

ENTRYPOINT ["java","-jar", "itbulat_institutions-0.0.1.jar"]

